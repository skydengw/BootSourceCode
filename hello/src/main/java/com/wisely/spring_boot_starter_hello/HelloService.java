package com.wisely.spring_boot_starter_hello;

/**
 * ${DESCRIPTION}
 *
 * @author wb-dw258721 dengw
 * @create 2018-01-11 14:58
 **/
public class HelloService {
    private String msg;
    public String sayHello(){

        return "Hello"+msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
